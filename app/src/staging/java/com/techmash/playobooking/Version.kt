package com.techmash.playobooking

object Version {
    const val PARTNER_APP_URL = "https://playo-partner-app-pwa-staging.web.app/"
}