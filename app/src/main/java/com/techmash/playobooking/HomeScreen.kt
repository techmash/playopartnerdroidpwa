package com.techmash.playobooking

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.webkit.*
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.onesignal.OneSignal
import com.techmash.playobooking.Version.PARTNER_APP_URL

class HomeScreen : AppCompatActivity() {

    private var _webView: WebView? = null

    private val webView: WebView
        get() = _webView!!

    private val pendingUrl: String? by lazy {
        intent.extras?.getString("url")
    }

    var permissionRequest: PermissionRequest? = null

    private val cameraPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            if (granted) {
                permissionRequest?.grant(arrayOf(PermissionRequest.RESOURCE_VIDEO_CAPTURE))
                Toast.makeText(this, "Granted", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "Denied", Toast.LENGTH_LONG).show()
            }
        }

    private val notificationPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            if (granted) {
                Log.d("NotificationPermission", "Granted")
            } else {
                Log.d("NotificationPermission", "Declined")
            }
        }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_screen)

        _webView = findViewById(R.id.web_view)

        webView.apply {
            webChromeClient = object : WebChromeClient() {
                override fun onPermissionRequest(request: PermissionRequest?) {
                    permissionRequest = request
                    val requestedResources = request?.resources ?: emptyArray()
                    for (r in requestedResources) {
                        Log.e("HomeScreen", "Permissions: $r")
                        if (r == PermissionRequest.RESOURCE_VIDEO_CAPTURE) {
                            if (ContextCompat.checkSelfPermission(
                                    this@HomeScreen,
                                    android.Manifest.permission.CAMERA
                                ) != PackageManager.PERMISSION_GRANTED
                            ) {
                                cameraPermission.launch(android.Manifest.permission.CAMERA)
                            } else {
                                permissionRequest?.grant(arrayOf(PermissionRequest.RESOURCE_VIDEO_CAPTURE))
                            }
                            break
                        }
                    }
                }

                override fun onProgressChanged(view: WebView?, newProgress: Int) {
                }
            }
            webViewClient = object : WebViewClient() {

                override fun shouldOverrideUrlLoading(
                    view: WebView,
                    request: WebResourceRequest
                ): Boolean {
                    Log.e(LOG_TAG, request.url.toString())
                    if (request.hasGesture() && request.url != null) {
                        Intent(Intent.ACTION_VIEW, request.url).also {
                            view.context?.startActivity(it)
                        }
                        return true
                    }
                    return super.shouldOverrideUrlLoading(view, request)
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    Log.e(LOG_TAG, url.toString())
                    CookieManager.getInstance().flush()
                    if (url != null && url.startsWith(PARTNER_APP_URL)) {
                        OneSignal.User.onesignalId.also { onesignalID ->
                            Log.e("Onesignal", onesignalID)
                            this@HomeScreen.webView
                                .evaluateJavascript(
                                    "window.localStorage.setItem('onesignalId','$onesignalID')",
                                    null
                                )
                        }
                    }
                }
            }

            settings.javaScriptEnabled = true
            settings.domStorageEnabled = true
            settings.cacheMode = WebSettings.LOAD_DEFAULT
            settings.allowFileAccess = true
            settings.mediaPlaybackRequiresUserGesture = false
        }
        Log.e(LOG_TAG, PARTNER_APP_URL)
        webView.loadUrl(pendingUrl ?: PARTNER_APP_URL)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val url = intent?.extras?.getString("url")
        if (url != null) {
            webView.loadUrl(url)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        // Check if the key event was the Back button and if there's history
        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
            webView.goBack()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.POST_NOTIFICATIONS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                notificationPermission.launch(android.Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        CookieManager.getInstance().flush()
    }

    override fun onDestroy() {
        super.onDestroy()
        _webView = null
    }

    companion object {
        val LOG_TAG = HomeScreen::class.simpleName
    }
}
